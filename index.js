const express = require('express');
const app = express();
const apiRouter = require('./routes/apiRouter');
const path = require('path');
const fs = require('fs');

app.use(express.json());

fs.stat(path.join(__dirname, './files'), (err) => {
    if (err) {
        fs.mkdir('files', () => {});
    }
    console.log('Folder was created');
});

app.use('/api', apiRouter);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});
