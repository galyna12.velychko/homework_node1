const { Router } = require('express');
const router = Router();
const path = require('path');
const fs = require('fs');

const dirPath = path.join(__dirname, '..', 'files');

router.post('/files', (req, res) => {
    const { filename, content } = req.body;
    if (!filename) {
        return res
            .status(400)
            .json({ message: 'Please specify filename parametr' });
    }
    if (!content) {
        return res
            .status(400)
            .json({ message: "Please specify 'content' parameter" });
    }

    const extentionFiles = path.extname(filename).slice(1);
    const allowedExtention = ['log', 'txt', 'json', 'yaml', 'xml'];
    if (allowedExtention.includes(extentionFiles)) {
        fs.writeFile(path.join(dirPath, filename), content, (err) => {
            if (err) {
                return res.status(500).json({ message: 'Server error' });
            } else {
                return res.status(200).json({
                    message: 'File created successfully',
                });
            }
        });
    } else {
        return res.status(400).json({
            message: `Thе extention ${extentionFiles} doesn't supported`,
        });
    }
    console.log(files);
});

router.get('/files', (_, res) => {
    fs.readdir(dirPath, (err, files) => {
        if (err) {
            return res.status(500).json({ message: 'Server error' });
        }
        if (files.length === 0) {
            console.log('The folder is empthy');
            return res.status(400).json({ message: 'Client error' });
        }
        console.log('Here a list of files');
        return res.status(200).json({ message: 'Success', files });
    });
});
router.get('/files/:filename', (req, res) => {
    const { filename } = req.params;
    console.log(filename);
    fs.readFile(path.join(dirPath, filename), 'utf-8', (err, data) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({
                    message: `No file with ${filename} filename found`,
                });
            }
            return res.status(500).json({ message: 'Server error' });
        } else {
            fs.stat(path.join(dirPath, filename), (err, stats) => {
                if (err) {
                    return res.status(500).json({ message: 'Server error' });
                } else {
                    return res.status(200).json({
                        message: 'Success',
                        filename,
                        data,
                        extension: path.extname(filename).slice(1),
                        uploadedDate: stats.birthtime,
                    });
                }
            });
        }
    });
});

router.put('/files/:filename', (req, res) => {
    const { filename } = req.params;
    const { newFileName, content } = req.body;
    fs.stat(path.join(dirPath, filename), (err) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({
                    message: `No file with ${filename} filename found`,
                });
            }
            return res.status(500).json({ message: 'Server error' });
        } else {
            if (newFileName) {
                fs.rename(
                    path.join(dirPath, filename),
                    path.join(dirPath, newFileName),
                    (err) => {
                        if (err) throw err;
                        console.log('файл перейменовано');
                    }
                );
            }
            if (content) {
                fs.writeFile(path.join(dirPath, filename), content, (err) => {
                    if (err) {
                        return res.status(500).json('Server error');
                    }
                    return res.status(200).json({
                        content,
                    });
                });
            }
        }
    });
});

router.delete('/files/:filename', (req, res) => {
    const { filename } = req.params;
    fs.unlink(path.join(dirPath, filename), (err) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({
                    message: `No file with ${filename} filename found`,
                });
            }
            return res.status(500).json({ message: 'Server error' });
        }
        console.log('фалй видалено');
        return res.status(200).json({ message: `The ${filename} was deleted` });
    });
});

module.exports = router;
